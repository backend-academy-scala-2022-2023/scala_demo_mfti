// class type parameters
trait Pet

class Cat extends Pet {
  def meowSound: String = "meow"
}

class Tarantool extends Pet

// T
case class PetShelter[T](val pets: Seq[T], val name: String)

// def shelterMeows(..): String =
// use method

def shelterMeows(shelter: PetShelter[Cat]): String =
  s"shelter: ${shelter.name}. Meows: ${shelter.pets.map(_.meowSound).mkString(", ")}"

val shelter = PetShelter[Cat](Seq(new Cat, new Cat), "Vasya")

shelterMeows(shelter)

// abstract type members
// trait PetShelterAbstract
// class CatShelterAbstract

trait PetShelterAbstract {
  type T
  val pets: Seq[T]
  val name: String
}

case class CatShelterAbstract(pets: Seq[Cat], name: String) extends PetShelterAbstract {
  type T = Cat
}

//


trait Film {
  trait Ticket {

  }


  def visit(ticket: Ticket): Unit = ()
  def buyTicket: Ticket = new Ticket {}
}

case class Surname(value: String) extends AnyVal


val filmA = new Film {}
val filmB = new Film {}
val filmC = new Film {}

//filmB.visit(filmA.buyTicket)
//filmA.visit(filmC.buyTicket)
filmA.visit(filmA.buyTicket)


// AT применяются в следующих случаях:
//  когда принципиально важно убрать TP из сигнатуры методов или типов.
// если TP имеют крайне сложный вид или их стало очень много и они делают код сложным для восприятия
//  что бы подчеркнуть, что AT по смыслу является частью типа в котором описан(отношение is-a)


// method type parameters
// def shelterMeows2
