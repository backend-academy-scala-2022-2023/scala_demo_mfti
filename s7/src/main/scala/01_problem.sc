// Generics problem
trait Pet

// PetShelter, pets, name

trait PetShelter {
  def pets: Seq[Pet]
  def name: String
}

// cat
class Cat extends Pet {
  def meowSound: String = "meow"
}

class Tarantula extends Pet


// shelterMeows

def shelterMeows(shelter: PetShelter): Unit = shelter.pets.foreach {
    case cat: Cat => println(cat.meowSound)
    case _ =>
}

val shelter = new PetShelter {
  override def pets = Seq(new Cat, new Tarantula)

  override def name = "Vasya"
}

shelterMeows(shelter)
// один из вариантов смотреть instance of, но он грязный, потому что runtime
// ?какие еще варианты?

//  <=