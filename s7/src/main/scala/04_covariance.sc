trait Pet

class Cat extends Pet {
  def meowSound: String = "meow"
}

class CatDog extends Cat {
  override def meowSound = "meoof"
  def burk: String = "woof"
}

class PetShelter[+T <: Pet](val pets: Seq[T], val name: String)

def shelterMeows(shelter: PetShelter[Cat]): String =
  s"""shelter: ${shelter.name}. Meows: ${shelter.pets.map(_.meowSound).mkString(", ")}"""

val catDogShelter = new PetShelter[CatDog](Seq(new CatDog), "cat dog shelter")

shelterMeows(catDogShelter)


val l1: Seq[Cat] = List(new Cat)
val l2: List[CatDog] = List(new CatDog)


val l3: Seq[Cat] = l1 ++ l2