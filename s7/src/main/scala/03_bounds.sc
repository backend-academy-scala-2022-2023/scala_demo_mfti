trait Pet

class Cat extends Pet {
  def meowSound: String = "meow"
}

class PetShelter[T](val pets: Seq[T], val name: String)

class Universe

val strangeShelter = new PetShelter[Universe](Seq(new Universe), "my shelter")


class BoundedPetShelter[T <: Pet](val pets: Seq[T], val name: String)

//val strangeShelter = new BoundedPetShelter[Universe](Seq(new Universe), "my shelter")

class CatDog extends Cat {
  override def meowSound: String = "meoof"
  def burk: String = "woof"
}

val catDogShelter = new PetShelter[CatDog](Seq(new CatDog), "cat dog shelter")
val catShelter = new PetShelter[Cat](Seq(new Cat), "cat shelter")

def shelterMeows(shelter: PetShelter[Cat]): String =
  s"shelter: ${shelter.name}. Meows: ${shelter.pets.map(_.meowSound).mkString(", ")}"

//shelterMeows(catDogShelter)

def shelterMeows2a[T <: Cat](shelter: PetShelter[T]): String =
  s"shelter: ${shelter.name}. Meows: ${shelter.pets.map(_.meowSound).mkString(", ")}"

def shelterMeows2b(shelter: PetShelter[_ <: Cat]): String =
  s"shelter: ${shelter.name}. Meows: ${shelter.pets.map(_.meowSound).mkString(", ")}"

shelterMeows2b(catDogShelter)
shelterMeows2b(catShelter)

// -----


trait A
class B extends A
class C extends A
class D extends C
// D -> C -> A
//      B _/

def f[T >: C](e: T): T = e
def ff[T >: C <: A](e: T): T = e
val f1: C = ff(new D)
val f2: A = ff(new B)
//val f3: Cat = ff(new Cat)