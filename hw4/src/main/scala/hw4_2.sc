import scala.annotation.tailrec

sealed trait Sex

case object Man extends Sex
case object Woman extends Sex

case class Tenant(age: Int, sex: Sex)

sealed trait Floor
case object Attic extends Floor
case class LivingFloor(bed1: Tenant, bed2: Tenant, next: Floor) extends Floor


case class Building(address: String, firstFloor: LivingFloor)

def protoFold(building: Building, acc0: Int)(f: (Int, LivingFloor) => Int): Int = {

  @tailrec
  def floorFold(floor: Floor, current: Int): Int = floor match {
    case Attic => current
    case livingFloor: LivingFloor => floorFold(livingFloor.next, f(current, livingFloor))
  }

  floorFold(building.firstFloor, acc0)

}


val building = Building("Baker Street",
  LivingFloor(
    Tenant(70, Man),
    Tenant(81, Man),
    LivingFloor(
      Tenant(12, Man),
      Tenant(13, Woman),
      LivingFloor(
        Tenant(50, Man),
        Tenant(23, Man),
        LivingFloor(
          Tenant(20, Woman),
          Tenant(39, Man), Attic
        )
      )
    )
  )
)

def countOldManFloors(building: Building, olderThen: Int): Int = protoFold(building, 0) {
  case (count, LivingFloor(Tenant(age, Man), _, _)) if age > olderThen => count + 1
  case (count, LivingFloor(_, Tenant(age, Man), _)) if age > olderThen => count + 1
  case (count, _) => count
}

countOldManFloors(building, 38)


import scala.math.max
def womanMaxAge(building: Building):Int = protoFold(building, 0) {
  case (current, LivingFloor(Tenant(rAge, Woman), Tenant(lAge, Woman), _)) => max(current, max(rAge, lAge))
  case (current, LivingFloor(Tenant(age, Woman), _, _)) => max(current, age)
  case (current, LivingFloor(_, Tenant(age, Woman), _)) => max(current, age)
  case (current, _) => current
}

womanMaxAge(building)