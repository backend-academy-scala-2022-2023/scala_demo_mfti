sealed trait Tree

case class Node(value: Int, left: Tree, right: Tree) extends Tree

case object RedLeaf extends Tree
case object YellowLeaf extends Tree
case object GreenLeaf extends Tree


def countYellowAndRedValues(tree: Tree): Int =
  tree match {
    case Node(value, YellowLeaf | RedLeaf, right) => value + countYellowAndRedValues(right)
    case Node(value, left, YellowLeaf | RedLeaf)  => value + countYellowAndRedValues(left)
    case Node(_, left, right)  => countYellowAndRedValues(left) + countYellowAndRedValues(right)
    case _ => 0
  }

import scala.math.max
def maxValue(tree: Tree): Option[Int] = tree match {
  case Node(value, left, right) =>
    val ml = maxValue(left).map(max(_, value))
    val mr = maxValue(right).map(max(_, value))
    ml.flatMap(l => mr.map(max(_, l)))
      .orElse(ml)
      .orElse(mr)
      .orElse(Some(value))
  case _ => None
}


val tree1 = RedLeaf
val tree2 = GreenLeaf
val tree3 = Node(1,
  YellowLeaf,
  Node(2,
    RedLeaf,
    GreenLeaf
  )
)

countYellowAndRedValues(tree1)
countYellowAndRedValues(tree2)
countYellowAndRedValues(tree3)


maxValue(tree1)
maxValue(tree2)
maxValue(tree3)