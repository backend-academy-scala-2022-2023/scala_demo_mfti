package demo

import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec

import java.time.{Clock, Instant, ZoneId}

class WorkingTimeServiceSpec extends AnyFlatSpec with MockFactory {

  trait Context {
    def now: String
    lazy val clock: Clock = mock[Clock]
    lazy val service      = new WorkingTimeService(clock)

    (() => clock.getZone()).expects().returns(ZoneId.of("UTC+3"))
    (() => clock.instant()).expects().onCall(() => Instant.parse(now))
  }

  it should "check if it working time" in new Context {
    override val now = "2007-12-03T10:15:30.00Z"
    assert(service.isWorkingTime())
  }

  it should "check if it not working time" in new Context {
    override val now = "2007-12-03T09:15:30.00Z"
    assert(service.isWorkingTime())
  }
}
