package demo

import org.scalatest.Inspectors
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should._

class RomanNumberParserSpec extends AnyFlatSpec with Matchers with Inspectors {
  "Roman numbers parser" should "return empty for empty string" in {
    new RomanNumberParser().parse("").isLeft shouldEqual true
  }

  it should "return 1 for I" in {
    new RomanNumberParser().parse("I") shouldBe Right(1)
  }

  it should "return 2,3 for II, III" in {
    val samples = Seq("II" -> 2, "III" -> 3)

    forAll(samples) { case (sample, expectation) =>
      new RomanNumberParser().parse(sample) shouldBe Right(expectation)
    }
  }

  it should "return 5 for V" in {
    new RomanNumberParser().parse("V") shouldBe Right(5)
  }

  it should "return 4 for IV" in {
    new RomanNumberParser().parse("IV") shouldBe Right(4)
  }

  it should "parse > 5 <= 10" ignore {
    val samples = Seq("VI" -> 6, "VII" -> 7, "VIII" -> 8, "IX" -> 9, "X" -> 10)

    forEvery(samples) { case (sample, expectation) =>
      new RomanNumberParser().parse(sample) shouldBe Right(expectation)
    }
  }

}
