trait Summable[T] {
  def zero: T
  def sum(a: T, b: T): T
}

object Summable {
  def apply[T](implicit inst: Summable[T]): Summable[T] = inst

  def instance[T](z: T, f: (T, T) => T): Summable[T] = new Summable[T] {
    override def zero = z

    override def sum(a: T, b: T) = f(a, b)
  }
}


//def sum(arr: Array[String]): String = arr.foldLeft("")(_ + _)
//def sum(arr: Array[Double]): Double = arr.foldLeft(0.0)(_ + _)
//def sum(arr: Array[Int]): Int = arr.foldLeft(0)(_ + _)

//def sum[T](arr: Array[T])(implicit S: Summable[T]): T = arr.foldLeft(S.zero)(S.sum)
def sum[T: Summable](arr: Array[T]): T = arr.foldLeft(Summable[T].zero)(Summable[T].sum)

implicit val intSummable = new Summable[Int] {
  override def zero = 0

  override def sum(a: Int, b: Int) = a + b
}

implicit val doubleSummable = new Summable[Double] {
  override def zero = 0.0

  override def sum(a: Double, b: Double) = a + b
}

implicit val stringSummable: Summable[String] = Summable.instance[String]("", _ + _)

implicit def listSummable[T]: Summable[List[T]] = Summable.instance(List.empty[T], _ ++ _)

sum(Array(1,2,3,4))
sum(Array(1.0,2.0,3.0,4.0))
sum(Array("1", "2", "3", "4"))
sum(Array(1 :: 2 :: Nil, 2:: Nil))
sum(Array("1" :: Nil, "2" :: Nil))