package demo

import java.time.{Clock, LocalTime}

class WorkingTimeService(clock: Clock = Clock.systemDefaultZone()) {
  def isWorkingTime(): Boolean = {
    val time = LocalTime.now(clock)

    if (time == LocalTime.parse("13:37")) {
      return false
    }

    time.isAfter(LocalTime.parse("10:00")) &&
    time.isBefore(LocalTime.parse("19:00"))
  }
}
