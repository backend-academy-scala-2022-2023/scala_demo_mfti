package demo

class RomanNumberParser {
  def parse(romanNumber: String): Either[String, Int] =
    romanNumber match {
      case ""    => Left("empty number")
      case "I"   => Right(1)
      case "II"  => Right(2)
      case "III" => Right(3)
      case "IV"  => Right(4)
      case "V"   => Right(5)
    }
}
