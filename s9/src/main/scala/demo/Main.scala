package demo

import cats.effect.{ExitCode, IO, IOApp}
import cats.{Applicative, Eval, Monad, Parallel}

object Main extends IOApp {
  import cats.syntax.applicative._
  import cats.syntax.functor._
  import cats.syntax.flatMap._
  import cats.syntax.parallel._
  import cats.syntax.traverse._

  def getName[F[_]: Applicative](id: Int): F[String] =
    s"User${id}".pure[F]

  def printNames[F[_]: Monad: Output](ids: List[Int]): F[Unit] =
    ids.traverse { id =>
      getName[F](id).flatMap(user => {
        val name = Thread.currentThread().getName
        Output[F].out(s"${name}: $user")
      })
    }.as("Finish")

  def printNamesPar[F[_]: Monad: Parallel: Output](ids: List[Int]): F[Unit] =
    ids.parTraverse { id =>
      getName[F](id).flatMap(user => {
        val name = Thread.currentThread().getName
        Output[F].out(s"${name}: $user")
      })
    }.as("Finish")

  val app = IO(println("Hello world"))

  val app2 = IO.parTraverseN(1)((1 to 100).toList) { id =>
    getName[IO](id).map(user => {
      val name = Thread.currentThread().getName
      println(s"${name}: $user")
    })
  }


  val app3 = (1 to 100).toList.parTraverse { id =>
    getName[IO](id).map(user => {
      val name = Thread.currentThread().getName
      println(s"${name}: $user")
    })
  }

  val app4 = printNames[IO]((1 to 100).toList)
  val app5 = printNames[Eval]((1 to 10).toList)

  println(app5.value)

//  val app7 = printNamesPar[Eval]((1 to 100).toList)
  val app6 = printNamesPar[IO]((1 to 10).toList)

  override def run(args: List[String]): IO[ExitCode] = {
    app6.as(ExitCode.Success)
  }
}
