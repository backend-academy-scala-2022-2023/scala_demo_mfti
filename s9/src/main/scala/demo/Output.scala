package demo

import cats.{Applicative, Defer}

trait Output[F[_]] {
  def out[A](value: A): F[Unit]
}

object Output {
  def apply[F[_]](implicit inst: Output[F]): Output[F] = inst

  class Console[F[_]:Defer: Applicative] extends Output[F] {
    override def out[A](value: A): F[Unit] = {
      Defer[F].defer {
        Applicative[F].pure(println(value))
      }
    }
  }

  implicit def outputF[F[_]:Defer: Applicative] = new Console[F]
}