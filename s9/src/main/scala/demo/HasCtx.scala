package demo

import cats.Applicative
import cats.data.ReaderT


trait HasCtx[F[_], Ctx] {
  def context: F[Ctx]
}

object HasCtx {
  implicit def readerCtx[F[_]: Applicative, Ctx]: HasCtx[ReaderT[F, Ctx, *], Ctx] = new HasCtx[ReaderT[F, Ctx, *], Ctx] {
    override def context: ReaderT[F, Ctx, Ctx] = ReaderT.ask[F, Ctx]
  }
}