package demo

import cats.Functor

trait TicketWindow[F[_]] {
  def buyTicket(money: Int, name: String): F[Ticket]
}

object TicketWindow {
  class Simple[F[_]: Functor: Output] extends TicketWindow[F] {
    import cats.syntax.functor._
    override def buyTicket(money: Int, name: String): F[Ticket] =
      Output[F].out(s"I am buy ticket on ${name}").as(Ticket(money, name))
  }
}

