package demo

import cats.data.ReaderT
import cats.effect.{ExitCode, IO, IOApp}
import cats.{Defer, Monad}

object Reader2Main extends IOApp {
  import cats.syntax.all._
  type Ctx = String
  type AppEnv[A] = ReaderT[IO, Ctx, A]

  class CtxOutput[F[_]: Monad: Defer](implicit ctx: HasCtx[F, Ctx]) extends Output[F] {
    private val console = new Output.Console[F]
    override def out[A](value: A): F[Unit] = for {
      c <- ctx.context
      _ <- console.out(s"${c}: ${value}")
    } yield ()
  }

  def happyWeekend[F[_]: Monad](ticketWindow: TicketWindow[F], cinema: Cinema[F]): F[Unit] = for {
    ticket <- ticketWindow.buyTicket(100, "Avatar 2")
    _ <- cinema.visit
    _ <- cinema.checkTicket(ticket)
  } yield ()

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val ctxOutput = new CtxOutput[AppEnv]
    val ticketWindow = new TicketWindow.Simple[AppEnv]
    val cinema = new Cinema.Simple[AppEnv]
    val weekend = happyWeekend(ticketWindow, cinema)

    List("Vasya", "Petya", "Masha")
      .parTraverse(weekend.run)
      .as(ExitCode.Success)
  }
}
