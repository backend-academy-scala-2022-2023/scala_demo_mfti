package demo

import cats.data.ReaderT
import cats.effect.{ExitCode, IO, IOApp}
import cats.{Defer, Monad}

import java.util.UUID

object ReaderMain extends IOApp {
  import cats.syntax.all._
  type Ctx = UUID
  type AppEnv[A] = ReaderT[IO, Ctx, A]



  class CtxOutput[F[_]: Monad: Defer](implicit ctx: HasCtx[F, Ctx]) extends Output[F] {
    private val console = new Output.Console[F]
    override def out[A](value: A): F[Unit] = for {
      c <- ctx.context
      _ <- console.out(s"${c}: ${value}")
    } yield ()
  }

  def buyTicket[F[_]: Monad: Output](cost: Int, eventName: String): F[Ticket] =
      Output[F].out(s"Ticket on ${eventName} cost=${cost}") >>
        Ticket(cost, eventName).pure[F]

  def visitEvent[F[_]:Monad: Output](ticket: Ticket)(implicit ctx: HasCtx[F, Ctx]): F[Unit] =
    ctx.context.flatMap(c =>
      Output[F].out(s"Visit ${c} ${ticket.eventName}")
    )

  def showTicket[F[_]: Output](ticket: Ticket): F[Unit] =
    Output[F].out(s"Show ticket for ${ticket.eventName}")

  def happyWeekend[F[_]: Monad: Output](implicit ctx: HasCtx[F, Ctx]): F[Unit] = for {
    ticket <- buyTicket[F](100, "Avatar 2")
    _ <- visitEvent[F](ticket)
    _ <- showTicket[F](ticket)
  } yield ()

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val ctxOutput = new CtxOutput[AppEnv]
    for {
//      _ <- happyWeekend[IO]
      _ <- happyWeekend[AppEnv].run(UUID.randomUUID())
      _ <- happyWeekend[AppEnv].run(UUID.randomUUID())
      _ <- happyWeekend[AppEnv].run(UUID.randomUUID())
    } yield ExitCode.Success
  }
}
