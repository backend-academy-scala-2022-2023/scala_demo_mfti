package demo

trait Cinema[F[_]] {
  def visit: F[Unit]
  def checkTicket(ticket: Ticket): F[Unit]
}

object Cinema {
  class Simple[F[_]: Output] extends Cinema[F] {
    override def visit: F[Unit] = Output[F].out("I am visit cinema")

    override def checkTicket(ticket: Ticket): F[Unit] = Output[F].out(s"I am check ticket on ${ticket.eventName}")
  }
}
