class Greeter(greeting: String, count: Int) {

  val name: String = "Greeter"
  private val n: String = "Greeter"

  def greet(userName: String): Unit = {
    (0 until  count).foreach { _ =>
      println(s"$greeting, $userName!")
    }
  }
  def len(userName: String): Int = userName.size
}

object Greeter {
  private val x = 1
  def apply(greeting: String): Greeter = {
    new Greeter(greeting, 1)
  }
  def apply(greeting: String, count: Int): Greeter = {
    new Greeter(greeting, count)
  }
  def twice(greeting: String): Greeter = {
    val x = new Greeter(greeting, 2)
    x.n
    x
  }
}


Greeter("Hello").greet("me")
Greeter("Hello", 2).greet("you")
//Greeter.apply("Hello").greet("me")

val greeter: Greeter = new Greeter("Hello", 1)
val greeter2: Greeter = new Greeter("Hello", 1)
val greeter3: Greeter = new Greeter("Hello", 1)

greeter.name
greeter.greet("Вася")
greeter greet "Вася"
greeter len "Вася" + 2

object Hello extends Greeter("Hello", 1)
object Hello2 extends Greeter("Hello", 1)

Hello.greet("Петя")

object Utils {
  val name = "Hi"
  def sum(x: Int, y: Int): Int = x + y
}

Utils.name

Utils.sum(1, 2)

greeter.hashCode()
greeter2.hashCode()
greeter3.hashCode()

greeter.toString

greeter == greeter2


case class Point(x: Int, y: Int) {
  def sum(point: Point) = Point(point.x + x, point.y + y)
}

object Point {

}

val p1 = Point(1, 2)
val p2 = Point(1, 2)

p1.hashCode()
p2.hashCode()

p1 == p2

p1.toString

p1.x
p1.y
p1.sum(p2)



