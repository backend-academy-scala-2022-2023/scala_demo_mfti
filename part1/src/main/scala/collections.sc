val seq  = Seq(1,2,3, 4)
val list = List(1,2, 3 ,4)

val set = Set(1, 2, 3, 3)

val map = Map(1 -> 1, 2 -> 2)

val eList = List.empty[Int]

// ----


list :+ 1
1 +: list

list :++ seq

seq ++: list


8 :: list

list match {
  case Nil => ???
  case head :: tail =>
    println(head, tail)
}

val mset = scala.collection.mutable.Set.empty[Int]
println(mset)
mset += 1
mset += 2
mset += 3
mset += 3


list.head
list.tail

list.headOption
List.empty[Int].headOption

1 :: 2 :: 3 :: Nil

// --------------

list.foreach(item => println(item))

list.map( _ * 2)
// 1 2 3 4 => 1 22 333 4444

list.map(i => List.fill(i)(i)).flatten


list.flatMap(i => List.fill(i)(i))

list.filter(_ % 2 == 0)
list.filterNot(_ % 2 == 0)
list.filter(_ % 2 != 0)

list.filter(_ % 2 == 0).map(_ * 10)

list.collect {
  case item if item % 2 == 0 => item * 10
}


trait Letter
trait Vowels {
  this : Letter =>
}

object Vowels {
  def unapply(arg: Letter): Option[Letter] =
    if(arg.isInstanceOf[Vowels]) Some(arg) else None
}

case object A extends Letter with Vowels
case object B extends Letter
case object C extends Letter
case object D extends Letter
case object E extends Letter with Vowels


val alphabet = List(A, B, C, D, E)

alphabet.filter {
  case _: Vowels => true
  case _ => false
}

alphabet.collect {
  case Vowels(a) => a
}

alphabet.find(_ == B)
alphabet.collectFirst({
  case Vowels(a) => a
})

// ----------------



