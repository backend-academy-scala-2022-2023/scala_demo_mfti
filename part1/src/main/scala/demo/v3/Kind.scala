package demo.v3

sealed trait Kind

object Kind {
  case object Client extends Kind
  case object Vip    extends Kind
  case object Staff  extends Kind
}
