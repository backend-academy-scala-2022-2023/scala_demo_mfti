package demo.v3

object Actions {
  def openMobileBank(obj: AnyRef): Unit =
    obj match {
      case user: User => println(s"User ${user.name} open mobile bank")
      case _          => println("Go away!")
    }

  def callPersonalManager(user: User): Unit =
    user.kind match {
      case Kind.Vip => println(s"User ${user.name} call personal manager")
      case _        => println("Go away!")
    }

  def switchOnFreeTariff(user: User): Unit =
    user match {
      case User(_, name, Kind.Staff) => println(s"User $name switched on free tariff")
      case _                         => println("Go away!")
    }
}
