package demo.v2

sealed trait Kind

object Kind {
  object Client extends Kind
  object Vip    extends Kind
  object Staff  extends Kind
}
