package demo.v2

object Actions {
  def openMobileBank(user: User): Unit =
    println(s"User ${user.name} open mobile bank")

  def callPersonalManager(user: User): Unit =
    if (user.kind == Kind.Vip) {
      println(s"User ${user.name} call personal manager")
    } else {
      println("Go away!")
    }

  def switchOnFreeTariff(user: User): Unit =
    if (user.kind == Kind.Staff) {
      println(s"User ${user.name} switched on free tariff")
    } else {
      println("Go away!")
    }
}
