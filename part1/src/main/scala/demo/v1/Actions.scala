package demo.v1

object Actions {
  def openMobileBank(user: User): Unit =
    println(s"User ${user.name} open mobile bank")

  def callPersonalManager(user: User): Unit =
    if (user.kind == "VIP") {
      println(s"User ${user.name} call personal manager")
    } else {
      println("Go away!")
    }

  def switchOnFreeTariff(user: User): Unit =
    if (user.kind == "staff") {
      println(s"User ${user.name} switched on free tariff")
    } else {
      println("Go away!")
    }
}
