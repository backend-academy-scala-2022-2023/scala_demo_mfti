package demo

object Main {
  def main(args: Array[String]): Unit =
    appV5

  def appV1 = {
    import v1._

    val client = User(1, "Вася", "client")
    val vip    = User(2, "Иван Иванович", "VIP")
    val staff  = User(3, "Олег", "staff")

    Actions.openMobileBank(client)
    Actions.openMobileBank(vip)
    Actions.openMobileBank(staff)

    println("-------")

    Actions.callPersonalManager(client)
    Actions.callPersonalManager(vip)
    Actions.callPersonalManager(staff)

    println("-------")

    Actions.switchOnFreeTariff(client)
    Actions.switchOnFreeTariff(vip)
    Actions.switchOnFreeTariff(staff)
  }

  def appV2 = {
    import v2._

    val client = User(1, "Вася", Kind.Client)
    val vip    = User(2, "Иван Иванович", Kind.Vip)
    val staff  = User(3, "Олег", Kind.Staff)

    Actions.openMobileBank(client)
    Actions.openMobileBank(vip)
    Actions.openMobileBank(staff)

    println("-------")

    Actions.callPersonalManager(client)
    Actions.callPersonalManager(vip)
    Actions.callPersonalManager(staff)

    println("-------")

    Actions.switchOnFreeTariff(client)
    Actions.switchOnFreeTariff(vip)
    Actions.switchOnFreeTariff(staff)
  }

  def appV3 = {
    import v3._

    val client = User(1, "Вася", Kind.Client)
    val vip    = User(2, "Иван Иванович", Kind.Vip)
    val staff  = User(3, "Олег", Kind.Staff)

    Actions.openMobileBank("Вася")
    Actions.openMobileBank(client)
    Actions.openMobileBank(vip)
    Actions.openMobileBank(staff)

    println("-------")

    Actions.callPersonalManager(client)
    Actions.callPersonalManager(vip)
    Actions.callPersonalManager(staff)

    println("-------")

    Actions.switchOnFreeTariff(client)
    Actions.switchOnFreeTariff(vip)
    Actions.switchOnFreeTariff(staff)
  }

  def appV4 = {
    import v4._

    val client = User.Client(1, "Вася")
    val vip    = User.Vip(2, "Иван Иванович", "Дмитрий")
    val staff  = User.Staff(3, "Олег", "oleg@tinkoff.ru")

    Actions.openMobileBank(client)
    Actions.openMobileBank(vip)
    Actions.openMobileBank(staff)

    println("-------")

//    Actions.callPersonalManager(client)
    Actions.callPersonalManager(vip)
//    Actions.callPersonalManager(staff)

    println("-------")

//    Actions.switchOnFreeTariff(client)
//    Actions.switchOnFreeTariff(vip)
    Actions.switchOnFreeTariff(staff)
  }

  def appV5 = {
    import v5._

    val client = User.Client(1, "Вася")
    val vip1   = User.Vip(2, "Иван Иванович", None)
    val staff  = User.Staff(4, "Олег", "oleg@tinkoff.ru")
    val vip2   = User.Vip(3, "Семен Семенович", Some(staff))
    val vip3   = User.Vip(5, "Иван Грозный", Some(vip1))

    Actions.openMobileBank(client)
    Actions.openMobileBank(vip1)
    Actions.openMobileBank(staff)

    println("-------")

    //    Actions.callPersonalManager(client)
    Actions.callPersonalManager(vip1)
    Actions.callPersonalManager(vip2)
    Actions.callPersonalManager(vip3)
    //    Actions.callPersonalManager(staff)

    println("-------")

    //    Actions.switchOnFreeTariff(client)
    //    Actions.switchOnFreeTariff(vip)
    Actions.switchOnFreeTariff(staff)

    val tree = Node(Node(Leaf(1), Leaf(2)), Leaf(3))

  }
}
