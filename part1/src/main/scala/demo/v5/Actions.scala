package demo.v5

object Actions {
  def openMobileBank(user: User): Unit = {
    val User.WithName(name) = user
    println(s"User $name open mobile bank")
  }

  def callPersonalManager(user: User.Vip): Unit =
    user.trustedUser.foreach { trustedUser =>
      println(s"User ${user.name} call personal manager ${trustedUser.name}")
      trustedUser match {
//          case u: User.Vip if u.trustedUser.nonEmpty => callPersonalManager(u)
//          case u@User.Vip(_, _, Some(_)) => callPersonalManager(u)
        case u @ User.Vip.WithTrusted() => callPersonalManager(u)
        case _                          => openMobileBank(trustedUser)
      }
    }

  def switchOnFreeTariff(user: User.Staff): Unit =
    println(s"User ${user.name} switched on free tariff")
}
