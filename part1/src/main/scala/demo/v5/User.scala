package demo.v5

sealed trait User {
  def id: Int
  def name: String
}

object User {
  case class Client(id: Int, name: String)                         extends User
  case class Vip(id: Int, name: String, trustedUser: Option[User]) extends User

  object Vip {
    object WithTrusted {
      def unapply(user: Vip): Boolean = user.trustedUser.isDefined
    }
  }

  case class Staff(id: Int, name: String, login: String) extends User

  object WithName {
    def unapply(user: User): Option[String] = Some(user.name)
  }

}

sealed trait Tree[A]
case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]
case class Leaf[A](value: A)                      extends Tree[A]
