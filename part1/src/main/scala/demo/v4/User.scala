package demo.v4

sealed trait User {
  def id: Int
  def name: String
}

object User {
  case class Client(id: Int, name: String)                       extends User
  case class Vip(id: Int, name: String, personalManager: String) extends User
  case class Staff(id: Int, name: String, login: String)         extends User
}
