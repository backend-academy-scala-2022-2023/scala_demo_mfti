package demo.v4

object Actions {
  def openMobileBank(user: User): Unit =
    println(s"User ${user.name} open mobile bank")

  def callPersonalManager(user: User.Vip): Unit =
    println(s"User ${user.name} call personal manager ${user.personalManager}")

  def switchOnFreeTariff(user: User.Staff): Unit =
    println(s"User ${user.name} switched on free tariff")
}
