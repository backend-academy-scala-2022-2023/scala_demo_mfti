import scala.io.Source
import java.nio.file.Paths
import scala.util.Using
println(Paths.get(".").toAbsolutePath)

val fn = "/Users/a.m.shcherbakov/develop/fintech/demo_x/.gitignore"
val lines = Using(Source.fromFile(fn)) { file =>
  file.getLines().toList
} .getOrElse(List.empty)

val rgx = "^\\..*/$".r
val rgx2 = "^\\.(\\w+)/$".r
val rgx2 = """^\.(\w+)/$""".r

lines.collect {
  case l@rgx() => l
}

lines.collect {
  case rgx2(v) => v
}


case class General()
case class Flags()
case class Stats()

trait Section

case class Response(id: Int,
                     general: Option[General],
                    flags: Option[Flags],
                    stats: Option[Stats]
                   )


type Sections = Map[String, Section]

trait SectionStorage {
  def getSection(storyId: Int, sections: Set[String]): Map[Int, Sections]
}

trait Builder[T] {
  def build(sections: Sections): Option[T]
}

class StatsBuilder extends Builder[Stats] {
  override def build(sections: Sections) = ???
}