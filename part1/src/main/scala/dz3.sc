case class Wealth(moneyAmount: Double, armyStrength: Int)

trait GreatHouse {
  def name: String
  def wealth: Wealth
}

//
//class X extends GreatHouse {
//  parent =>
//  override def name = ???
//
//  override def wealth =  ???
//
//  def foo: GreatHouse = new GreatHouse {
//    override def name = ???
//
//    override def wealth = parent.wealth
//  }
//}


trait MakeWildFire {
  this: GreatHouse =>
  def makeWildFire: Wealth = wealth.copy(armyStrength = wealth.armyStrength + 200)
}

trait BorrowMoney {
  house: GreatHouse =>
  def borrowMoney: Wealth = house.wealth.copy(moneyAmount = house.wealth.moneyAmount + 1000)
}

trait CallDragon {
  this: GreatHouse =>
  def callDragon: Wealth = wealth.copy(armyStrength = wealth.armyStrength * 2)
}

case class Lannisters(wealth: Wealth) extends GreatHouse with BorrowMoney with MakeWildFire {
  override val name = "Lannisters"
}

case class Targaryen(wealth: Wealth) extends GreatHouse with BorrowMoney with CallDragon {
  override val name = "Targaryen"
}

case class GameOfThrones(lannisters: Lannisters, targaryen: Targaryen, turn: Int = 0) {
  def nextTurn(lannistersTurn: Lannisters => Wealth)(targaryenTurn: Targaryen => Wealth): GameOfThrones = {
    GameOfThrones(
      lannisters.copy(wealth = lannistersTurn(lannisters)),
      targaryen.copy(wealth = targaryenTurn(targaryen)),
      turn = turn + 1
    )
  }
}


val gameOfThrones = GameOfThrones(
  lannisters = Lannisters(Wealth(moneyAmount = 100, armyStrength = 300)),
  targaryen = Targaryen(Wealth(moneyAmount = 100, armyStrength = 100)),
)

gameOfThrones
  .nextTurn(_.borrowMoney)(_.callDragon)
  .nextTurn(_.makeWildFire)(_.borrowMoney)