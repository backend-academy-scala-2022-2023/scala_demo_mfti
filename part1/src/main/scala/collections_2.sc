val list = List(1, 3, 2,4)

for {
  a <- list
} println(a)

for {
  a <- list
  b <- list
} println(a, b)

for {
  a <- list
  b <- list
  c <- list
} yield a + b  + c

for {
  _ <- Set(1)
  a <- list
} yield a


list.contains(2)
list.contains(9)
list.contains("")

list.exists(_ % 2 == 0)
list.forall(_ % 2 == 0)

List.empty[Int].exists(_ % 2 == 0)
List.empty[Int].forall(_ % 2 == 0)



list.sorted
list.sorted(Ordering.Int.reverse)


case class Point(x: Int, y: Int)

val points = List(Point(1, 2), Point(4, 5), Point(3, 7), Point(4, 2))

points.sortBy(_.x)
points.sortBy(_.y)


points.sortBy(p => (p.x, p.y))
points.sortBy(p => (p.y, p.x))

list.zip(points)

//points.zipWithIndex.foreach {
//  case (p, idx ) => _
//}

points.groupBy(_.x)
points.groupBy(_.x).map {
  case (k, v) => k -> v.length
}

points.groupBy(_.x).map {
  case (k, v) => k -> v.map(_.y)
}

val map = points.groupMap(_.x)(_.y)

list.sliding(2, 1).toList
list.sliding(2, 1).map(_.sum).toList
list.sliding(2, 1).map(_.sum / 2.0).toList

map(3)
//map(2)
map.get(2)
map.get(1)

