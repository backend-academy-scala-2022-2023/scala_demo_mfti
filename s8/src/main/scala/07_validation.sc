import cats.data.{Validated, ValidatedNel}
import scala.util.{Failure, Success, Try}

import cats.syntax.apply._
import cats.syntax.validated._


case class RegistrationData(firstName: String,
                            surname: String,
                            age: Option[String])

case class User(firstName: String,
                surname: String,
                age: Int)

case class DataError(msg: String) extends Exception(msg)

type ValidationResult[A] = ValidatedNel[DataError, A]

  def validateFirstName(name: String): ValidationResult[String] = {
    if (name.length >= 2 && name.length <= 256) name.validNel
    else DataError("Name too short or too long").invalidNel
  }

  def validateSurname(name: String): ValidationResult[String] = {
    if (name.length >= 2 && name.length <= 256) name.validNel
    else DataError("Name too short or too long").invalidNel
  }

  def validateAge(age: Option[String]): ValidationResult[Int] =
    age.map(i => Try(i.toInt)) match {
      case Some(value) => value match {
        case Success(value) if value >= 18 => value.validNel
        case Failure(_) =>  DataError("invalid number").invalidNel
        case _ => DataError("too young").invalidNel
      }
      case None => DataError("age is required").invalidNel
    }

val data = RegistrationData("1", "bb", Some("x12321"))


val user: ValidationResult[User] = (
  validateFirstName(data.firstName),
  validateSurname(data.surname),
  validateAge(data.age)
).mapN((fname, sname, age) => User(fname, sname, age))

println(user)
val user = for {
  fname <- validateFirstName(data.firstName).toEither
  sname <- validateSurname(data.surname).toEither
  age <- validateAge(data.age).toEither
} yield User(fname, sname, age)

println(user)
