package demo

object Main extends App {
  def builder(ev: MyEval[Int], flag: Boolean): MyEval[Int] = {
    if(flag) ev.map(_ * 3)
    else ev.map(_ + 4).map(_ => throw new Exception("!!!!"))
  }

  val e = MyEval.const("hello").map(h => println(s"$h world"))
  val calc = builder(MyEval(1), true).flatMap(x => MyEval(x * 2)).flatMap(x => MyEval(println(s"x=${x}")))
  val calc2 = builder(MyEval(1), false).map(_ + 2).map(x => println(s"x=${x}"))
  println("== START ==")
  println(e)
  e.run
  e.run
  println("== START 2 ==")
  println(calc)
  calc.run
  calc.run
  calc.run
  println("== START 3 ==")
  println(calc2)
  println(calc2.run)
  println(calc2.run)

  val res = for {
    x <- MyEval(1)
    y <- MyEval(2)
  } yield x + y

  println(res.run)
}
