package demo

import scala.util.{Failure, Success, Try}


trait MyEval[A] {
  def run: Try[A]
  def flatMap[B](f: A => MyEval[B]): MyEval[B]
  def map[B](f: A => B): MyEval[B] = flatMap(value => ConstMyEval(f(value)))
}

object MyEval {
  def const[A](value: A): MyEval[A] = ConstMyEval(value)
  def apply[A](value: => A): MyEval[A] = DeferredMyEval(
    () => const(value)
  )
}

case class FailedMyEval[A](throwable: Throwable) extends MyEval[A] {
  override val run: Try[A] = Failure(throwable)

  override def flatMap[B](f: A => MyEval[B]): MyEval[B] = FailedMyEval(throwable)
}

case class ConstMyEval[A](value: A) extends MyEval[A] {
  override def run: Try[A] = Success(value)

  override def flatMap[B](f: A => MyEval[B]): MyEval[B] =
      DeferredMyEval(() => Try(f(value)) match {
        case Failure(exception) => FailedMyEval(exception)
        case Success(value) => value
      })
}

case class DeferredMyEval[A](value: () => MyEval[A]) extends MyEval[A] {
  override def run: Try[A] = value().run

  override def flatMap[B](f: A => MyEval[B]): MyEval[B] = {
      DeferredMyEval(() => value().flatMap(f))
  }
}