import scala.collection.mutable.ArrayBuffer


class Compressor {
  def compress(numbers: Seq[Int]): Seq[(Int, Int)] = {
    numbers.foldLeft(ArrayBuffer[(Int, Int)]()) { (acc, elem) =>
      acc.lastOption match {
        case Some(last -> count) if last == elem =>
          acc(acc.size - 1) = (elem, count + 1)
          acc
        case Some(last -> count) if last != elem =>
          acc.addOne(elem, 1)
          acc
        case None =>
          acc.addOne(elem, 1)
          acc
      }
    }.toSeq
  }
}

val compressor = new Compressor

val input = Seq(1, 2, 2, 3, 3, 3, 4, 2, 2)
val output = Seq((1, 1), (2, 2), (3, 3), (4, 1))
println(compressor.compress(input))