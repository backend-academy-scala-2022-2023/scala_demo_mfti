class Compressor {
  def compress(numbers: Seq[Int]): Seq[(Int, Int)] = {
//    numbers.foldLeft(List[(Int, Int)]()) {
//      case (Nil, elem) => (elem -> 1) :: Nil
//      case ((last -> counter) :: tail, elem) if elem == last =>
//        (elem -> (counter+1)) :: tail
//      case (acc@(last -> counter) :: tail, elem) if elem != last =>
//        (elem -> 1) :: acc
//    }

    numbers.foldLeft(List[(Int, Int)]()) {
      case ((last -> counter) :: tail, elem) if elem == last =>
        (elem -> (counter+1)) :: tail
      case (acc, elem) => (elem -> 1) :: acc
    } .reverse
  }
}

val compressor = new Compressor

val input = Seq(1, 2, 2, 3, 3, 3, 4, 2, 2)
val output = Seq((1, 1), (2, 2), (3, 3), (4, 1))
println(compressor.compress(input))