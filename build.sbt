import sbt.Keys.libraryDependencies

ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .aggregate(demo1, demo2, hw4, hw5, s10, s11)
  .settings(coverageFailOnMinimum := false, coverageMinimumStmtTotal := 90, coverageMinimumBranchTotal := 90)

lazy val demo1 = (project in file("part1"))
  .settings(name := "demo_mfti_part1", dependencyOverrides ++= Seq(), libraryDependencies ++= Seq())

lazy val demo2 = (project in file("part2"))
  .settings(
    name := "demo_mfti_part2",
    dependencyOverrides ++= Seq(),
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
    libraryDependencies += "org.scalamock" %% "scalamock" % "5.1.0"  % Test
  )

lazy val hw4 = (project in file("hw4"))
  .settings(name := "demo_mfti_hw4")

lazy val hw5 = (project in file("hw5"))
  .settings(name := "demo_mfti_hw5")

lazy val s7 = (project in file("s7"))
  .settings(name := "mfti_s7")

lazy val s8 = (project in file("s8"))
  .settings(name := "mfti_s8", libraryDependencies += "org.typelevel" %% "cats-core" % "2.8.0")

lazy val s9 = (project in file("s9"))
  .settings(
    name := "mfti_s9",
    libraryDependencies += "org.typelevel" %% "cats-core"   % "2.8.0",
    libraryDependencies += "org.typelevel" %% "cats-effect" % "3.4.0",
    libraryDependencies += compilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
  )

lazy val s10 = (project in file("s10"))
  .settings(
    name := "mfti_s10",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
    libraryDependencies += "org.scalamock" %% "scalamock" % "5.1.0"  % Test
  )
  .enablePlugins(JavaAppPackaging)

lazy val s11 = (project in file("s11"))
  .settings(
    name := "mfti_s11",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
    libraryDependencies += "org.scalamock" %% "scalamock" % "5.1.0"  % Test
  )
