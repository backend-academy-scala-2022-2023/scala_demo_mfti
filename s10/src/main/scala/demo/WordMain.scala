package demo

import demo.backend.{FileWordRepository, InMemoryWordRepository, LoggableWordRepository}
import demo.frontend.ConsoleFrontend

import java.nio.file.{Files, Paths}

object WordMain extends App {
//  new ConsoleFrontend(LoggableWordRepository.wrap(new InMemoryWordRepository)).start()
  new ConsoleFrontend(LoggableWordRepository.wrap(new FileWordRepository(Paths.get("./words.txt")))).start()
}
