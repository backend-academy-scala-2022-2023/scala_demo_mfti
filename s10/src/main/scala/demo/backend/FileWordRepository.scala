package demo.backend

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, StandardOpenOption}
import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters._

class FileWordRepository(file: Path) extends WordRepository {
  override def get(word: String): Option[String] =
    Files.lines(file).filter(word.equals).findFirst().toScala

  override def put(word: String): Unit =
    Files.write(file, s"$word\n".getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND, StandardOpenOption.CREATE)

  override def delete(word: String): Unit = {
    val lines = Files.readAllLines(file).asScala
    Files.write(file, lines.filterNot(word.equals).asJava)
  }
}
