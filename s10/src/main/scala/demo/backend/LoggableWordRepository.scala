package demo.backend

class LoggableWordRepository private (underlying: WordRepository, prefix: Option[String]) extends WordRepository {
  override def get(word: String): Option[String] = underlying.get(word)

  override def put(word: String): Unit = {
    underlying.put(word)
    println(s"${prefix.getOrElse("")} Stored word: $word")
  }

  override def delete(word: String): Unit = {
    underlying.delete(word)
    println(s"${prefix.getOrElse("")} Removed word: $word")
  }
}

object LoggableWordRepository {
  def wrap(wordRepository: WordRepository): WordRepository =
    new LoggableWordRepository(wordRepository, None)

  def wrapWithPrefix(wordRepository: WordRepository, prefix: String): WordRepository =
    new LoggableWordRepository(wordRepository, Some(prefix))
}
