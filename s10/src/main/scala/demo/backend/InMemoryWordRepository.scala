package demo.backend

import scala.collection.mutable

class InMemoryWordRepository extends WordRepository {
  private val storage = new mutable.HashSet[String]()

  override def get(word: String): Option[String] = storage.find(_ == word)

  override def put(word: String): Unit = storage.add(word)

  override def delete(word: String): Unit = storage.remove(word)
}
