package demo.frontend

import demo.backend.WordRepository

import scala.io.StdIn.readLine
import scala.util.matching.Regex

class ConsoleFrontend(backend: WordRepository) extends Frontend {
  private val commandAnWord: Regex = "^(get|put|delete)\\s+(.*)$".r

  override def start(): Unit =
    while (true) {
      val str = readLine()
      str match {
        case commandAnWord("get", word)    => println(backend.get(word))
        case commandAnWord("put", word)    => backend.put(word)
        case commandAnWord("delete", word) => backend.delete(word)
        case "quit"                        => return
        case _                             => println("Unknown command, please use: 'get', 'put', 'delete', or quit")
      }
    }

}
