package demo.backend

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

trait WordRepositoryBehaviors { this: AnyFlatSpec with Matchers =>

  def validWordRepository(testedRepository: => WordRepository): Unit = {
    trait Ctx {
      val repository: WordRepository = testedRepository
    }

    it should "return now word if empty" in new Ctx {
      repository.get("some word") shouldBe empty
    }

    it should "return added word" in new Ctx {
      repository.put("added word")
      repository.get("added word") shouldBe Some("added word")
    }

    it should "delete added word" in new Ctx {
      repository.put("deleted word")
      repository.delete("deleted word")
      repository.get("deleted word") shouldBe empty
    }

    it should "store word only once" in new Ctx {
      repository.put("uniq word")
      repository.put("uniq word")
      repository.delete("uniq word")
      repository.get("uniq word") shouldBe empty
    }

    it should "store multiple words" in new Ctx {
      repository.put("some word A")
      repository.put("some word B")
      repository.get("some word A") shouldBe Some("some word A")
      repository.get("some word B") shouldBe Some("some word B")
    }
  }

}
