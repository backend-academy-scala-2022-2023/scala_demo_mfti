package demo.backend

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Files

class WordRepositorySpec extends AnyFlatSpec with Matchers with WordRepositoryBehaviors {
  behavior of "In memory word repository"
  it should behave like validWordRepository(new InMemoryWordRepository)

  behavior of "File word repository"
  it should behave like validWordRepository(new FileWordRepository(Files.createTempFile(null, null)))
}
