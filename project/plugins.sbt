// https://github.com/scalameta/sbt-scalafmt
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.6")

// https://github.com/scoverage/sbt-scoverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.9.3")

// https://www.scala-sbt.org/sbt-native-packager/

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.1")
