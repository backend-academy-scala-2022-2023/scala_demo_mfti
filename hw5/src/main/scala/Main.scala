import scala.io.Source
import scala.util.Using

object Main extends App {
  case class Student(lastName: String, marks: List[Int])

  def median(s: Seq[Int]): Double = {
    val (lower, upper) = s.sorted.splitAt(s.size / 2)
    if (s.size % 2 == 0) (lower.last + upper.head) / 2.0 else upper.head
  }

  def moda(s: Seq[Int]): Int =
    s.groupMapReduce(identity)(_ => 1)(_ + _)
      .toSeq
      .sortBy(_._2)(Ordering[Int].reverse)
      .head
      ._1

  def average(s: Seq[Int]): Double =
//    (0.0 + s.sum) / s.length
    ((0.0 + s.sum) / s.length * 100).round / 100.0

  val nameMarkPattern = "(\\w+):(\\d+)".r

  val nameMarks = Using(Source.fromFile("hw5/marks.txt")) { source =>
    source
      .getLines()
      .map { case nameMarkPattern(name, mark) =>
        (name, mark.toInt)
      }
      .toSeq
      .groupMapReduce(_._1)(pair => Seq(pair._2))(_ ++ _)
      .map { case (k, v) =>
        Student(k, v.toList)
      }
      .toList
  }.get

  val sumMarks = nameMarks.map(pair => pair.lastName -> pair.marks.sum)

  println("-------SUM--------")
  for {
    (name, sum) <- sumMarks
  } println(s"$name: $sum")

  println("-------SORT--------")
  for {
    (name, sum) <- sumMarks.sortBy(_._2)(Ordering.Int.reverse)
//    (name, sum) <- sumMarks.toSeq.sortBy(-_._2)
  } println(s"$name: $sum")

  println("-------AVG--------")
  for {
    student <- nameMarks
  } println(s"${student.lastName}: ${average(student.marks)}")
//  } println(f"$name: ${average(marks)}%.2f")

  val marks = nameMarks.flatMap(_.marks)
  println(s"""|-------MARKS METRICS--------
              |Sum all: ${marks.sum}
              |Median: ${median(marks)}
              |Moda: ${moda(marks)}""".stripMargin)

  case class X()

  val x = X()

}
