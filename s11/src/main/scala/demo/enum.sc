object Color extends Enumeration {
  val Red, Green, Blue = Value
}

object Size extends Enumeration {
  val S, M, L, XL = Value
}

def f(color: Color.Value) = println(color)
def f(size: Size.Value)   = println(size)

//val x: List[Color.Value] = List(Color.Red, Size.XL)

def nonExhaustive(color: Color.Value) = color match {
  case Color.Red   => println("Red")
  case Color.Green => println("Green")
}

nonExhaustive(Color.Blue)
