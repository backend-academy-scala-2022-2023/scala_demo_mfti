package demo

import demo.commands._
import demo.ui._
import demo.strategy._

object Main extends App {

  val job = Pipeline(List(Step.Mul(2), Step.Plus(2), Step.Mul(3), Step.Debug[Int](_.toString)))

  job.run(10)

  val panel = Panel("Panel1", None) {
    case Event.Trace =>
      println("I'm Panel1")
      false
    case Event.Click(_, _) =>
      println("Click on panel")
      true
  }
  val button = Button("Button", Some(panel)) {
    case Event.Trace =>
      println("I'm Button")
      false
    case Event.Click(5, 5) =>
      println("Click on button")
      true
    case _ => false
  }

  button.event(Event.Trace)
  button.event(Event.Click(1, 2))
  button.event(Event.Click(5, 5))

  val us1 = UrlShortenerByStrategy(Strategy.ByUUID)
  val us2 = UrlShortenerByStrategy(Strategy.ByRandom)

  println(us1.shortUrl("http://habr.ru"))
  println(us2.shortUrl("http://habr.ru"))

}
