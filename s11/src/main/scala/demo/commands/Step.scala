package demo.commands

trait Step[T] {
  def apply(in: T): Either[String, T]
}

object Step {
  class Mul[T: Numeric](k: T) extends Step[T] {
    override def apply(in: T): Either[String, T] = Right(Numeric[T].times(in, k))
  }

  object Mul {
    def apply[T: Numeric](k: T): Mul[T] = new Mul(k)
  }

  class Plus[T: Numeric](k: T) extends Step[T] {
    override def apply(in: T): Either[String, T] = Right(Numeric[T].plus(in, k))
  }

  object Plus {
    def apply[T: Numeric](k: T): Plus[T] = new Plus(k)
  }

  class Debug[T](f: T => String = (_: T).toString) extends Step[T] {
    override def apply(in: T): Either[String, T] = {
      println(s"DEBUG: ${f(in)}")
      Right(in)
    }
  }

  object Debug {
    def apply[T](f: T => String): Debug[T] = new Debug(f)
  }
}
