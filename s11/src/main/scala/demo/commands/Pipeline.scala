package demo.commands

import scala.annotation.tailrec

case class Pipeline[T](steps: List[Step[T]]) {
  def run(value: T): Either[String, T] = {
    @tailrec
    def helper(currentValue: T, lefSteps: List[Step[T]]): Either[String, T] =
      lefSteps match {
        case head :: nextSteps =>
          head(currentValue) match {
            case Left(value)     => Left(value)
            case Right(newValue) => helper(newValue, nextSteps)
          }
        case Nil => Right(currentValue)
      }

    helper(value, steps)
  }
}
