package demo.ui

sealed trait Event

object Event {
  case object Trace                extends Event
  case class Click(x: Int, y: Int) extends Event
}
