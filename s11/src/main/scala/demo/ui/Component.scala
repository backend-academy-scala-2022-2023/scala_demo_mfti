package demo.ui

trait Component {
  def parent: Option[Component]

  final def event(event: Event): Unit =
    if (!onEvent(event)) parent.foreach(_.event(event))

  def onEvent(event: Event): Boolean
}

class Button(title: String, val parent: Option[Component], handler: Event => Boolean) extends Component {
  override def onEvent(event: Event): Boolean = handler(event)
}

object Button {
  def apply(title: String, parent: Option[Component])(handler: Event => Boolean) = new Button(title, parent, handler)
}

class Panel(title: String, val parent: Option[Component], handler: Event => Boolean) extends Component {
  override def onEvent(event: Event): Boolean = handler(event)
}

object Panel {
  def apply(title: String, parent: Option[Component])(handler: Event => Boolean) = new Panel(title, parent, handler)
}
