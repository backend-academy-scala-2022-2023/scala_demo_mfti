package demo.strategy

import java.util.UUID
import scala.util.Random

trait Strategy {
  def genShortId: String
}

object Strategy {

  object ByUUID extends Strategy {
    override def genShortId: String = UUID.randomUUID().toString
  }

  object ByRandom extends Strategy {
    val chars: Array[Char] = ('a' to 'z').toArray
    override def genShortId: String =
      List.fill(8)(chars(Random.nextInt(chars.length))).mkString("")

  }
}
