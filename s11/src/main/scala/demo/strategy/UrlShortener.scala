package demo.strategy

trait UrlShortener {
  def shortUrl(url: String): String
}

class UrlShortenerByStrategy(strategy: Strategy) extends UrlShortener {
  override def shortUrl(url: String): String = {
    val id = strategy.genShortId
    s"https://l.tinkoff.ru/$id"
  }
}

object UrlShortenerByStrategy {
  def apply(strategy: Strategy): UrlShortenerByStrategy = new UrlShortenerByStrategy(strategy)
}
